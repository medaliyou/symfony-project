<?php


namespace App\Controller\Account;



use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class AccountController extends AbstractController
{
    /**
     * @Route("/account", name="app_account")
     */
    public function account(SerializerInterface $serializer)
    {
        $user = $serializer->serialize($this->getUser(), 'json' );
        return new JsonResponse(json_decode($user));
    }
}