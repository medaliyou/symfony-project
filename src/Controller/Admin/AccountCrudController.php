<?php


namespace App\Controller\Admin;


use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\SerializerInterface;

class AccountCrudController extends AbstractController
{

    /**
     * @Route("/api/v1/accounts" , name="app_api_accounts", methods={"GET"})
     */
    public function accounts(SerializerInterface $serializer)
    {
        $accounts = $this->getDoctrine()->getRepository(User::class)->findAll();
        $json_accounts = $serializer->serialize($accounts, 'json');
        return new JsonResponse(json_decode($json_accounts));
    }

    /**
     * @Route("/api/v1/accounts/{id}", name="app_api_accounts_get", methods={"GET"}, requirements={"id" = "\d+"})
     */
    public function accountsById(SerializerInterface $serializer, $id)
    {
        $accounts = $this->getDoctrine()->getRepository(User::class)->find($id);
        $json_accounts = $serializer->serialize($accounts, 'json');
        return new JsonResponse(json_decode($json_accounts));
    }

    /**
     * @Route("/api/v1/accounts/replace/{id}" , name="app_api_accounts_update", methods={"PUT"}, requirements={"id" = "\d+"})
     */
    public function accountEdit(UserPasswordEncoderInterface $passwordEncoder, SerializerInterface $serializer, Request $request, $id)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        if (!$user) {
            throw $this->createNotFoundException(sprintf(
                'No user found with id "%s"',
                $id
            ));
        }

        $data = json_decode($request->getContent(), 'json');
        empty($data['apiToken']) ? true : $user->setApiToken($data['apiToken']);
        empty($data['email']) ? true : $user->setEmail($data['email']);
        empty($data['roles']) ? true : $user->setRoles($data['roles']);
        empty($data['password']) ? true : $user->setPassword(
            $passwordEncoder->encodePassword(
                $user,
                $data['password']
            )
        );

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        $ret = $serializer->serialize($user, 'json');
        return new JsonResponse(json_decode($ret), 200);

    }



}