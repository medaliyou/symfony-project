<?php

namespace App\Controller\Book;

use App\Entity\Book;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class BookCrudController extends AbstractController
{

    /**
     * @Route ("/api/v1/book/pages/{min}/{max}",name="app_api_book_pages_range")
     */
    public function findByPagesInRange(EntityManagerInterface $entityManager , $min,$max): Response
    {
        $rep = $entityManager->getRepository(Book::class);
        $arr=$rep->findByPagesInRange($min,$max);
        $serializer = SerializerBuilder::create()->build();

        $books=$serializer->serialize($arr,'json');
        return new JsonResponse(json_decode($books));

    }

    /**
     * @Route ("/api/v1/book/dateover/{min}",name="app_api_book_dateover")
     * @param EntityManagerInterface $entityManager
     * @param $min
     * @return Response
     */
    public function findByDateOver (EntityManagerInterface $entityManager , $min): Response
    {
        $rep = $entityManager->getRepository(Book::class);
        $arr=$rep->findByDateOver($min);
        $serializer = SerializerBuilder::create()->build();

        $books=$serializer->serialize($arr,'json');
        return new JsonResponse(json_decode($books));

    }
    /**
     * @Route ("/api/v1/books",name="app_api_books")
     * @return Response
     */
    public function findBooks(SerializerInterface $serializer):Response{
        $rep=$this->getDoctrine()->getRepository(Book::class);
        $arr=$rep->findAll();

        $serializer = SerializerBuilder::create()->build();

        $books=$serializer->serialize($arr,'json');
        return new JsonResponse(json_decode($books));
    }


    /**
     * @Route ("/api/v1/book/title/{title}" , name="app_api_book_title")
     * @param $title
     */
    public function findBookByTitle($title){
        $rep=$this->getDoctrine()->getRepository(Book::class);
        $arr=$rep->findBy(['title'=>$title]);
        $serializer = SerializerBuilder::create()->build();

        $books=$serializer->serialize($arr,'json');
        return new JsonResponse(json_decode($books));

    }
    /**
     * @Route ("/api/v1/book/isbn/{isbn}",name="app_api_book_isbn" )
     *
     */
    public function findBookByIsbn($isbn){
        $rep=$this->getDoctrine()->getRepository(Book::class);
        $arr=$rep->findBy(['isbn'=>$isbn]);
        $serializer = SerializerBuilder::create()->build();

        $books=$serializer->serialize($arr,'json');
        return new JsonResponse(json_decode($books));

    }
    /**
     * @Route ("/api/v1/book/pages/{nbPages}",name="app_api_book_pages",requirements={"nbPages" = "\d+"})
     */
    public function findBookByPages($nbPages){
        $rep=$this->getDoctrine()->getRepository(Book::class);
        $arr=$rep->findBy(['pages'=>$nbPages]);
        $serializer = SerializerBuilder::create()->build();

        $books=$serializer->serialize($arr,'json');
        return new JsonResponse(json_decode($books));

    }

    /**
     * @Route ("/api/v1/book/price/{price}",name="app_api_book_price",requirements={"price" = "\d+"})
     * @param $price
     * @return JsonResponse
     */
    public function findBookByPrice($price){
        $rep=$this->getDoctrine()->getRepository(Book::class);
        $arr=$rep->findBy(['price'=>$price]);
        $serializer = SerializerBuilder::create()->build();

        $books=$serializer->serialize($arr,'json');
        return new JsonResponse(json_decode($books));
    }

    /**
     * @Route ("/api/v1/book/price/{min}/{max}",name="app_api_book_price_range",requirements={"min" = "\d+","max" = "\d+"})
     * @param EntityManagerInterface $entityManager
     * @param $min
     * @param $max
     * @return Response
     */
    public function findByPriceInRange(EntityManagerInterface $entityManager , $min,$max): Response
    {
        $rep = $entityManager->getRepository(Book::class);
        $arr=$rep->findByPriceInRange($min,$max);
        $serializer = SerializerBuilder::create()->build();

        $books=$serializer->serialize($arr,'json');
        return new JsonResponse(json_decode($books));
    }

    /**
     * @Route ("/api/v1/book/price/min/{min}",name="app_api_book_price_range_min")
     * @param EntityManagerInterface $entityManager
     * @param SerializerInterface $serializer
     * @param $min
     * @return Response
     */
    public function findByPriceOver (EntityManagerInterface $entityManager,SerializerInterface $serializer , $min): Response
    {
        $rep = $entityManager->getRepository(Book::class);
        $arr=$rep->findByPriceOver($min);
        $serializer = SerializerBuilder::create()->build();

        $books=$serializer->serialize($arr,'json');
        return new JsonResponse(json_decode($books));
    }

    /**
     * @Route ("/api/v1/book/price/max/{max}" , name="app_api_book_price_range_max")
     * @param EntityManagerInterface $entityManager
     * @param $max
     * @return Response
     */
    public function findByPriceUnder (EntityManagerInterface $entityManager , $max): Response
    {
        $rep = $entityManager->getRepository(Book::class);
        $arr=$rep->findByPriceUnder($max);
        $serializer = SerializerBuilder::create()->build();

        $books=$serializer->serialize($arr,'json');
        return new JsonResponse(json_decode($books));
    }

    /**
     * @Route ("/api/v1/book/picture/{picture}",name="app_api_book_picture")
     * @param $picture
     * @return JsonResponse
     */
    public function findBookByPicture($picture){
        $rep=$this->getDoctrine()->getRepository(Book::class);
        $arr=$rep->findBy(['picture'=>$picture]);

        $serializer = SerializerBuilder::create()->build();

        $books=$serializer->serialize($arr,'json');

        return new JsonResponse(json_decode($books));
    }



}
