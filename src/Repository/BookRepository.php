<?php

namespace App\Repository;

use App\Entity\Book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Book[]    findAll()
 * @method Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Book::class);
    }

    // /**
    //  * @return Book[] Returns an array of Book objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Book
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    } */

    public function findByPagesInRange ($min , $max) : array {
        $constReq=$this->createQueryBuilder('b')
            ->where('b.pages>= :min')
            ->andWhere('b.pages<= :max')
            ->setParameter('min',$min)
            ->setParameter('max',$max)
            ->orderBy('b.pages','DESC');
        $request=$constReq->getQuery();
        return $request->execute();
    }
    public function findByDateOver ($min ) : array {
        $constReq=$this->createQueryBuilder('b')
            ->where('b.editionDate >= :min')
            ->setParameter('min',$min)
            ->orderBy('b.editionDate','ASC');
        $requete=$constReq->getQuery();
        return $requete->execute();
    }
    public function findByPriceInRange ($min , $max) : array {
        $constReq=$this->createQueryBuilder('b')
            ->where('b.price>= :min')
            ->andWhere('b.price<= :max')
            ->setParameter('min',$min)
            ->setParameter('max',$max)
            ->orderBy('b.price','DESC');
        $request=$constReq->getQuery();
        return $request->execute();
    }
    public function findByPriceOver ($min ) : array {
        $constReq=$this->createQueryBuilder('b')
            ->where('b.price>= :min')
            ->setParameter('min',$min)
            ->orderBy('b.price','DESC');
        $request=$constReq->getQuery();
        return $request->execute();
    }
    public function findByPriceUnder ($max ) : array {
        $constReq=$this->createQueryBuilder('b')
            ->where('b.price<= :max')
            ->setParameter('max',$max);
        $request=$constReq->getQuery();
        return $request->execute();
    }
    

}
